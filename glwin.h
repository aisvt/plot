
									// Header File For Windows
#include "IGLWidget.h"
#include <windowsx.h>

/*-------------------------------------------------------------------------------------------------*/
											/*GLOBAL*/
/*-------------------------------------------------------------------------------------------------*/

HGLRC	hRC=NULL;							   				// Permanent Rendering Context
HDC		hDC=NULL;											// Private GDI Device Context
HWND	hWnd=NULL;											// Holds Our Window Handle
HINSTANCE	hInstance;										// Holds The Instance Of The Application

IGLWidget *gl = CreateGLWidget();

bool keys[256];												// Array Used For The Keyboard Routine
bool active=TRUE;											// Window Active Flag Set To TRUE By Default
bool fullscreen=TRUE;										// Fullscreen Flag Set To Fullscreen Mode By Default

GLvoid KillGLWindow(GLvoid);
BOOL CreateGLWindow(LPWSTR title, int width, int height, int bits, bool fullscreenflag);
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);		// Declaration For WndProc
