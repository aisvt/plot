//---------------------------------------------------------------------------


#pragma hdrstop

#include "glwidget.h"
#include <iostream>
//---------------------------------------------------------------------------
#define DEBUG              // delete on release
#pragma package(smart_init)

IGLWidget *CreateGLWidget()
{
    return new GLWidget();
}

GLWidget::GLWidget() : w(0), h(0)
{
	//setFormat(QGLFormat(QGL::SingleBuffer));
	initializeGL();
}

GLWidget::~GLWidget(){
	 this->clearScene();
}

void GLWidget::clearScene(){
#ifdef DEBUG
	std::cout << "glwidget::deleting gobjects"<<std::endl;
#endif
	for(map<string, GraphicalObject*>::iterator i =
			objects.begin(); i != objects.end(); i++){
		delete i->second;
	}
#ifdef DEBUG
	std::cout << "glwidget::clearing collection"<<std::endl;
#endif
	objects.clear();
}

void GLWidget::GLpaint()
{
	this -> paintGL();
}
void GLWidget::GLinitialize()
{
	this -> initializeGL();
}
void GLWidget::GLresize(int nw, int nh)
{
	this -> resizeGL(this->w = nw, this->h = nh);
}
void GLWidget::GLupdate()
{
	// this -> updateGL();
	this -> GLpaint();
}

void GLWidget::initializeGL() {
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	//glViewport(0,0, width(), height());
	// call resize() instead
}

void GLWidget::paintGL(){

#ifdef DEBUG
	std::cout << "paintGL()"<<std::endl;
#endif

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	for(std::map<string, GraphicalObject*>::const_iterator i = this->objects.begin(); i != this->objects.end(); ++i){
		i->second->draw();
	}


	glFlush();
}

void GLWidget::resizeGL(int nw, int nh){
#ifdef DEBUG
	std::cout << "resizeGL()"<<std::endl;
#endif
	glViewport(0,0, this->w = nw, this->h = nh);
}

void GLWidget::mouseMoveEvent(int xPos, int yPos){

	for(std::vector<IMouseEventListener*>::const_iterator i = this->mouseListeners.begin(); i != mouseListeners.end(); ++i){
		(*i)->mouseMoveEvent(xPos, yPos);
	}
}

void GLWidget::mouseReleaseEvent(int xPos, int yPos){

	for(std::vector<IMouseEventListener*>::const_iterator i = this->mouseListeners.begin(); i != mouseListeners.end(); ++i){
		(*i)->mouseReleaseEvent(xPos, yPos);
	}
}

void GLWidget::mousePressedEvent(int xPos, int yPos){
    for(std::vector<IMouseEventListener*>::const_iterator i = this->mouseListeners.begin(); i != mouseListeners.end(); ++i){
		(*i)->mousePressedEvent(xPos, yPos);
	}
}


void GLWidget::wheelEvent(int delta){

	for(std::vector<IMouseEventListener*>::const_iterator i = this->mouseListeners.begin(); i != mouseListeners.end(); i++){
		(*i)->mouseScrollEvent(delta);
	}
}

void GLWidget::addObject(string key, GraphicalObject *p){
	this->objects[key] = p;
}

double GLWidget::trScreenToGLx(int x){
	return (double)(this->w - x)/this->w;
}

double GLWidget::trScreenToGLy(int y){
	return (double)(this->h - y)/this->h;
}

int GLWidget::trGLToScreenx(double x){
	return (double)(this->w) * (x - 1.0);
}

int GLWidget::trGLToScreeny(double y){
	return (double)(this->w) * (y - 1.0);
}

void GLWidget::subscribeToMouse(IMouseEventListener*l){
	this->mouseListeners.push_back(l);
}

void GLWidget::setWorkingArea(double xmin, double xmax, double ymin, double ymax){
#ifdef DEBUG
	std::cout << "setWorkingArea("<<xmin<<","<<xmax<<","<<ymin<<","<<ymax<<")"<<std::endl;
#endif
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(xmin, xmax, ymin, ymax, -1.0, 0.0);
}

void GLWidget::removeWithPrefix(string key){

#ifdef DEBUG
	std::cout << "removeWithPrefix(){"<<std::endl;
#endif
	map<string, GraphicalObject*>::iterator it = objects.begin();
	vector<map<string, GraphicalObject*>::iterator> toErase;
	for(; it != objects.end(); it++){
		if(it->first.substr(0,key.size()) == key){
			toErase.push_back(it);
#ifdef DEBUG
	std::cout << "found: " << it->first.c_str()<<std::endl;
#endif

		}
	}
	for(std::vector<map<string, GraphicalObject*>::iterator>::iterator i = 
			toErase.begin(); i != toErase.end(); i++){
		objects.erase(*i);
	}
	
#ifdef DEBUG
	std::cout << "}"<<std::endl;
#endif
}

void GLWidget::removeAt(int){
}

int GLWidget::width()
{
	return this->w;
}
int GLWidget::height()
{
    return this->h;
}
