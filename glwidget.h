//---------------------------------------------------------------------------

#ifndef glwidgetH
#define glwidgetH



//---------------------------------------------------------------------------
#include <map>
#include <string>
#include <vector>

#include "IGLWidget.h"
#include "event.h"
#include "aglwidget.h"
#include "graphical_object.h"

using std::string;
using std::vector;
using std::map;

class GLWidget : public IGLWidget, public AGLWidget
{
public:
	GLWidget();
	virtual ~GLWidget();
private:
	// gl funcs	 /* ex Qt part */
	void initializeGL();
	void resizeGL(int nw, int nh);
	void paintGL();
public:
	//Adapter part
	virtual void GLpaint();
	virtual void GLresize(int nw, int nh);
	virtual void GLupdate();
	virtual void GLinitialize();
	virtual void mousePressedEvent(int xPos, int yPos);
	virtual void mouseMoveEvent(int xPos, int yPos);
	virtual void mouseReleaseEvent(int xPos, int yPos);
	virtual void wheelEvent(int delta);
	//Adapter part
	virtual void subscribeToMouse(IMouseEventListener*);

	virtual double trScreenToGLx(int);
	virtual double trScreenToGLy(int);
	virtual int trGLToScreenx(double);
	virtual int trGLToScreeny(double);

	virtual void addObject(string, GraphicalObject*);
        virtual void removeWithPrefix(string);
	virtual void removeAt(int);
	virtual void clearScene();


	virtual void setWorkingArea(double xmin, double xmax, double ymin, double ymax);

public:
	int width();
	int height();
private:
	int mouseX, mouseY;
	map<string, GraphicalObject*> objects;
	vector<IMouseEventListener*> mouseListeners;
	int w;
        int h;
};




#endif // GLWIDGET_H

