//---------------------------------------------------------------------------

#include "glwin.h"
#include "kbplot.h"
#pragma hdrstop

#include <tchar.h>
#include <math.h>
#include <stdlib.h>
#include <time.h>
//---------------------------------------------------------------------------

/*-------------------------------------------------------------------------------------------------*/
											/*GL*/
/*-------------------------------------------------------------------------------------------------*/

GLvoid ReSizeGLScene(GLsizei width, GLsizei height)			// Resize And Initialize The GL Window
{
	if (height==0)											// Prevent A Divide By Zero By
        height=1;
	gl->GLresize(width, height);
}

int InitGL(GLvoid)											// Setup For OpenGL
{
	gl->GLinitialize();

	return TRUE;
}

int DrawGLScene(GLvoid)
{
	gl->GLpaint();

	return TRUE;
}

/*-------------------------------------------------------------------------------------------------*/
											/*MAIN*/
/*-------------------------------------------------------------------------------------------------*/

#pragma argsused
WINAPI _tWinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPTSTR lpCmdLine, int nCmdShow)
{

	MSG msg;									// Windows Message Structure
	BOOL    done=FALSE;                         // Bool Variable To Exit Loop

	// Ask The User Which Screen Mode They Prefer
	if (MessageBox(NULL,"Would You Like To Run In Fullscreen Mode?", "Start FullScreen?",MB_YESNO|MB_ICONQUESTION)==IDNO)
	{
		fullscreen=FALSE;                       // Windowed Mode
	}

	// Create Our OpenGL Window
	if (!CreateGLWindow(L"OpenGL Window",640,480,16,fullscreen))
	{
		return 0;								// Quit If Window Was Not Created
	}

		KbPlot p(dynamic_cast<GLWidget*>(gl), -10.0, 50.0, -1.0, 50.0);

	/*-----------------------test1 (by hand)---------------------------------*/
	  std::vector<Txy> *v = new std::vector<Txy>();
	v->push_back(Txy(0.0,0.0));
	v->push_back(Txy(1.0,1.0));
     v->push_back(Txy(-1.0,1.0));
    v->push_back(Txy(2.0,4.0));
     v->push_back(Txy(-2.0,4.0));
	v->push_back(Txy(3.0,9.0));
	v->push_back(Txy(-3.0,9.0));
	DataSet ds(v);
	 Style s;//  ------------
	 s.lineColor = 0xAC0000FF ;  //-------------
	 s.lineThickness = 3.0;//
	 s.markerSize = 20;//
	 s.markerColor = 0x00BFFF;  //
	s.markerType = Style::MARK_TRIANGLE;//
	s.lineStroke = Style::LINE_STD;//
		p.draw(ds,s);
  /*----------------test2(sequence)----------------------*/
	double data[256];
    for (int i = 0; i < 128; i+=2) {
	   data[i] = i;
	   // data[i+1] = log(i*i*i);
	   data[i+1] = (i*i*i);
	}
	DataSet ds1(RawData::get((unsigned char*)(&data[2]),50, 16, 0, 8, RawData::TYPE_DOUBLE));
	Style s1;
	s1.lineColor = 0x800080 ;  //   6555FFFF
	s1.lineThickness = 3.0;
	s1.lineStroke = Style::LINE_DASHED;
	p.toggleGrid(true);
	p.setGridColor(0x00BFFF); //   0x77777777
	p.setGridYStroke(Style::LINE_DOTTED);
	std::vector<string>  strM;
	strM.push_back("1");
	strM.push_back("2");
	strM.push_back("3");
	strM.push_back("4");
	strM.push_back("5");
	strM.push_back("6");
	strM.push_back("7");
	p.draw(ds1, strM, s1);
 //--------------------test 3 (Random)---------------
	 std::vector<Txy> *v1 = new std::vector<Txy>();
  srand(time(NULL));
	for(int i = 0; i <30; i++)
	{
	double k = 2.0 + rand()%100;
		double m = 5.0 +rand()%50;
			 v1->push_back(Txy(k,m));
	}
	DataSet ds3(v1);
	Style s3;//  ------------
	s3.lineColor = 0xAC0000FF ;  //-------------
	s3.lineThickness = 1.0;//
	s3.lineStroke = Style::LINE_DOTTED;
	s3.markerSize = 8;//
	s3.markerColor = 0x00BFFF;  //
	s3.markerType = Style::MARK_SQUARE;//
	p.draw(ds3,s3);

 //---------------------------------------


	while(!done)                                // Loop That Runs Until done=TRUE
	{
		if (PeekMessage(&msg,NULL,0,0,PM_REMOVE))           // Is There A Message Waiting?
		{
			if (msg.message==WM_QUIT)						// Have We Received A Quit Message?
			{
				done=TRUE;                  // If So done=TRUE
			}
			else                            // If Not, Deal With Window Messages
			{
				TranslateMessage(&msg);             // Translate The Message
				DispatchMessage(&msg);				// Dispatch The Message
			}
		}
		else                                // If There Are No Messages
		{
			// Draw The Scene.  Watch For ESC Key And Quit Messages From DrawGLScene()
			if (active)                     // Program Active?
			{
				if (keys[VK_ESCAPE])        // Was ESC Pressed?
				{
					done=TRUE;              // ESC Signalled A Quit
				}
				else                        // Not Time To Quit, Update Screen
				{
					DrawGLScene();          // Draw The Scene
					SwapBuffers(hDC);       // Swap Buffers (Double Buffering)
				}
			}

			if (keys[VK_F1])                // Is F1 Being Pressed?
			{
				keys[VK_F1]=FALSE;					// If So Make Key FALSE
				KillGLWindow();						// Kill Our Current Window
				fullscreen=!fullscreen;				// Toggle Fullscreen / Windowed Mode
				// Recreate Our OpenGL Window
				if (!CreateGLWindow(L"OpenGL Window",640,480,16,fullscreen))
				{
					return 0;						// Quit If Window Was Not Created
				}
			}
		}
	}

	 // Shutdown
    KillGLWindow();									// Kill The Window
	return (msg.wParam);                            // Exit The Program
}
//---------------------------------------------------------------------------
 /*-------------------------------------------------------------------------------------------------*/
											/*WINDOW*/
/*-------------------------------------------------------------------------------------------------*/

GLvoid KillGLWindow(GLvoid)									// Properly Kill The Window
{
        delete gl;
        
	if (fullscreen)
	{
		ChangeDisplaySettings(NULL,0);						// Switch Back To The Desktop
		ShowCursor(TRUE);									// Show Mouse Pointer
	}

	if (hRC)								// Do We Have A Rendering Context?
	{
				glFinish();
                //glFlush();
		if (!wglMakeCurrent(NULL,NULL))     // Are We Able To Release The DC And RC Contexts?
			MessageBox(NULL,"Release Of DC And RC Failed.","SHUTDOWN ERROR",MB_OK | MB_ICONINFORMATION);

		if (!wglDeleteContext(hRC))         // Are We Able To Delete The RC?
			MessageBox(NULL,"Release Rendering Context Failed.","SHUTDOWN ERROR",MB_OK | MB_ICONINFORMATION);

		hRC=NULL;                           // Set RC To NULL
	}

	if (hDC && !ReleaseDC(hWnd,hDC))					// Are We Able To Release The DC
	{
		MessageBox(NULL,"Release Device Context Failed.","SHUTDOWN ERROR",MB_OK | MB_ICONINFORMATION);
		hDC=NULL;										// Set DC To NULL
	}

	if (hWnd && !DestroyWindow(hWnd))       // Are We Able To Destroy The Window?
	{
		MessageBox(NULL,"Could Not Release hWnd.","SHUTDOWN ERROR",MB_OK | MB_ICONINFORMATION);
		hWnd=NULL;                          // Set hWnd To NULL
	}

	if (!UnregisterClass("OpenGL",hInstance))   // Are We Able To Unregister Class
	{
		MessageBox(NULL,"Could Not Unregister Class.","SHUTDOWN ERROR",MB_OK | MB_ICONINFORMATION);
        hInstance=NULL;                         // Set hInstance To NULL
    }
}

BOOL CreateGLWindow(wchar_t* title, int width, int height, int bits, bool fullscreenflag)
{
	GLuint      PixelFormat;					// Holds The Results After Searching For A Match
	WNDCLASS    wc;								// Windows Class Structure

	DWORD       dwExStyle;                      // Window Extended Style
	DWORD       dwStyle;                        // Window Style

	static char appName[] = "KBPlot";

	RECT WindowRect;                            // Grabs Rectangle Upper Left / Lower Right Values
	WindowRect.left=(long)0;                    // Set Left Value To 0
	WindowRect.right=(long)width;               // Set Right Value To Requested Width
	WindowRect.top=(long)0;                     // Set Top Value To 0
	WindowRect.bottom=(long)height;             // Set Bottom Value To Requested Height

	fullscreen=fullscreenflag;                  // Set The Global Fullscreen Flag

	hInstance = GetModuleHandle(NULL);					// Grab An Instance For Our Window
	wc.style  = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;		// Redraw On Move, And Own DC For Window
	wc.lpfnWndProc = (WNDPROC) WndProc;					// WndProc Handles Messages
	wc.cbClsExtra = 0;									// No Extra Window Data
	wc.cbWndExtra = 0;									// No Extra Window Data
	wc.hInstance = hInstance;							// Set The Instance
	wc.hIcon = LoadIcon(NULL, IDI_WINLOGO);				// Load The Default Icon
	wc.hCursor = LoadCursor(NULL, IDC_ARROW);			// Load The Arrow Pointer
	wc.hbrBackground = NULL;							// No Background Required For GL
	wc.lpszMenuName = NULL;								// We Don't Want A Menu
	wc.lpszClassName = "OpenGL";						// Set The Class Name

	if (!RegisterClass(&wc))							// Attempt To Register The Window Class
	{
		MessageBox(NULL,"Failed To Register The Window Class.","ERROR",MB_OK|MB_ICONEXCLAMATION);
		return FALSE;									// Exit And Return FALSE
	}

	if (fullscreen)										// Attempt Fullscreen Mode?
	{
		DEVMODE dmScreenSettings;						// Device Mode
		memset(&dmScreenSettings,0,sizeof(dmScreenSettings));       // Makes Sure Memory's Cleared
		dmScreenSettings.dmSize=sizeof(dmScreenSettings);			// Size Of The Devmode Structure
		dmScreenSettings.dmPelsWidth    = width;					// Selected Screen Width
		dmScreenSettings.dmPelsHeight   = height;					// Selected Screen Height
		dmScreenSettings.dmBitsPerPel   = bits;						// Selected Bits Per Pixel
		dmScreenSettings.dmFields=DM_BITSPERPEL|DM_PELSWIDTH|DM_PELSHEIGHT;

		// Try To Set Selected Mode And Get Results.  NOTE: CDS_FULLSCREEN Gets Rid Of Start Bar.
		if (ChangeDisplaySettings(&dmScreenSettings,CDS_FULLSCREEN)!=DISP_CHANGE_SUCCESSFUL)
		{
			// If The Mode Fails, Offer Two Options.  Quit Or Run In A Window.
			if (MessageBox(NULL,"The Requested Fullscreen Mode Is Not Supported By\nYour Video Card. Use Windowed Mode Instead?","Warning",MB_YESNO|MB_ICONEXCLAMATION)==IDYES)
			{
				fullscreen=FALSE;               // Select Windowed Mode (Fullscreen=FALSE)
			}
			else
			{
				// Pop Up A Message Box Letting User Know The Program Is Closing.
				MessageBox(NULL,"Program Will Now Close.","ERROR",MB_OK|MB_ICONSTOP);
				return FALSE;                   // Exit And Return FALSE
			}
		}
	}

	if (fullscreen)                             // Are We Still In Fullscreen Mode?
	{
		dwExStyle=WS_EX_APPWINDOW;              // Window Extended Style
		dwStyle=WS_POPUP;                       // Windows Style
		ShowCursor(FALSE);                      // Hide Mouse Pointer
	}
	else
	{
		dwExStyle=WS_EX_APPWINDOW | WS_EX_WINDOWEDGE;   // Window Extended Style
		dwStyle=WS_OVERLAPPEDWINDOW;                    // Windows Style
	}

	AdjustWindowRectEx(&WindowRect, dwStyle, FALSE, dwExStyle);     // Adjust Window To True Requested Size

	if (!(hWnd=CreateWindowEx(  dwExStyle,              // Extended Style For The Window
				"OpenGL",								    // Class Name
				appName,								// Window Title
				WS_CLIPSIBLINGS |						// Required Window Style
				WS_CLIPCHILDREN |						// Required Window Style
				dwStyle,								// Selected Window Style
                0, 0,									// Window Position
				WindowRect.right-WindowRect.left,		// Calculate Adjusted Window Width
                WindowRect.bottom-WindowRect.top,		// Calculate Adjusted Window Height
                NULL,									// No Parent Window
                NULL,									// No Menu
                hInstance,								// Instance
				NULL)))									// Don't Pass Anything To WM_CREATE

				{
					KillGLWindow();                         // Reset The Display
					MessageBox(NULL,"Window Creation Error.","ERROR",MB_OK|MB_ICONEXCLAMATION);
					return FALSE;                           // Return FALSE
				}

				static  PIXELFORMATDESCRIPTOR pfd=                  // pfd Tells Windows How We Want Things To Be
				{
					sizeof(PIXELFORMATDESCRIPTOR),                  // Size Of This Pixel Format Descriptor
					1,												// Version Number
					PFD_DRAW_TO_WINDOW |							// Format Must Support Window
					PFD_SUPPORT_OPENGL |							// Format Must Support OpenGL
					PFD_DOUBLEBUFFER,								// Must Support Double Buffering
					PFD_TYPE_RGBA,									// Request An RGBA Format
					bits,											// Select Our Color Depth
					0, 0, 0, 0, 0, 0,								// Color Bits Ignored
					0,												// No Alpha Buffer
					0,												// Shift Bit Ignored
					0,												// No Accumulation Buffer
					0, 0, 0, 0,										// Accumulation Bits Ignored
					16,												// 16Bit Z-Buffer (Depth Buffer)
					0,												// No Stencil Buffer
					0,												// No Auxiliary Buffer
					PFD_MAIN_PLANE,									// Main Drawing Layer
					0,												// Reserved
					0, 0, 0											// Layer Masks Ignored
				};

				if (!(hDC=GetDC(hWnd)))								// Did We Get A Device Context?
				{
					KillGLWindow();									// Reset The Display
					MessageBox(NULL,"Can't Create A GL Device Context.","ERROR",MB_OK|MB_ICONEXCLAMATION);
					return FALSE;									// Return FALSE
				}

				if (!(PixelFormat=ChoosePixelFormat(hDC,&pfd)))     // Did Windows Find A Matching Pixel Format?
				{
					KillGLWindow();									// Reset The Display
					MessageBox(NULL,"Can't Find A Suitable PixelFormat.","ERROR",MB_OK|MB_ICONEXCLAMATION);
					return FALSE;									// Return FALSE
				}

				if(!SetPixelFormat(hDC,PixelFormat,&pfd))           // Are We Able To Set The Pixel Format?
				{
					KillGLWindow();									// Reset The Display
					MessageBox(NULL,"Can't Set The PixelFormat.","ERROR",MB_OK|MB_ICONEXCLAMATION);
					return FALSE;									// Return FALSE
				}

				if (!(hRC=wglCreateContext(hDC)))                   // Are We Able To Get A Rendering Context?
				{
					KillGLWindow();									// Reset The Display
					MessageBox(NULL,"Can't Create A GL Rendering Context.","ERROR",MB_OK|MB_ICONEXCLAMATION);
					return FALSE;									// Return FALSE
				}

				if(!wglMakeCurrent(hDC,hRC))                        // Try To Activate The Rendering Context
				{
					KillGLWindow();									// Reset The Display
					MessageBox(NULL,"Can't Activate The GL Rendering Context.","ERROR",MB_OK|MB_ICONEXCLAMATION);
					return FALSE;									// Return FALSE
				}

				ShowWindow(hWnd,SW_SHOW);							// Show The Window
				SetForegroundWindow(hWnd);							// Slightly Higher Priority
				SetFocus(hWnd);										// Sets Keyboard Focus To The Window
				ReSizeGLScene(width, height);                       // Set Up Our Perspective GL Screen

				if (!InitGL())										// Initialize Our Newly Created GL Window
				{
					KillGLWindow();									// Reset The Display
					MessageBox(NULL,"Initialization Failed.","ERROR",MB_OK|MB_ICONEXCLAMATION);
					return FALSE;									// Return FALSE
				}

				return TRUE;										// Success
}

LRESULT CALLBACK WndProc(   HWND    hWnd,       // Handle For This Window
                UINT    uMsg,                   // Message For This Window
                WPARAM  wParam,                 // Additional Message Information
                LPARAM  lParam)                 // Additional Message Information
{
	switch (uMsg)                               // Check For Windows Messages
	{
		case WM_ACTIVATE:                       // Watch For Window Activate Message
		{
			// LoWord Can Be WA_INACTIVE, WA_ACTIVE, WA_CLICKACTIVE,
			// The High-Order Word Specifies The Minimized State Of The Window Being Activated Or Deactivated.
			// A NonZero Value Indicates The Window Is Minimized.
			if ((LOWORD(wParam) != WA_INACTIVE) && !((BOOL)HIWORD(wParam)))
				active=TRUE;						// Program Is Active
			else
				active=FALSE;						// Program Is No Longer Active

			return 0;								// Return To The Message Loop
		}

		case WM_SYSCOMMAND:                     // Intercept System Commands
		{
			switch (wParam)                     // Check System Calls
			{
				case SC_SCREENSAVE:             // Screensaver Trying To Start?
				case SC_MONITORPOWER:           // Monitor Trying To Enter Powersave?
				return 0;						// Prevent From Happening
			}
			break;								// Exit
		}

		case WM_CLOSE:                          // Did We Receive A Close Message?
		{
			PostQuitMessage(0);                 // Send A Quit Message
			return 0;							// Jump Back
		}

		case WM_KEYDOWN:                        // Is A Key Being Held Down?
		{
			keys[wParam] = TRUE;                // If So, Mark It As TRUE
			return 0;							// Jump Back
		}

		case WM_KEYUP:                          // Has A Key Been Released?
		{
			keys[wParam] = FALSE;               // If So, Mark It As FALSE
			return 0;							// Jump Back
		}

		case WM_SIZE:                           // Resize The OpenGL Window
		{
			ReSizeGLScene(LOWORD(lParam),HIWORD(lParam));       // LoWord=Width, HiWord=Height
			return 0;							// Jump Back
		}

		case WM_LBUTTONDOWN:
		{
        	short xPos = GET_X_LPARAM(lParam);
			short yPos = GET_Y_LPARAM(lParam);
			gl->mousePressedEvent(xPos, yPos);
			return 0;
		}

		case WM_LBUTTONUP:
		{
			short xPos = GET_X_LPARAM(lParam);
			short yPos = GET_Y_LPARAM(lParam);
			gl->mouseReleaseEvent(xPos, yPos);
			return 0;
		}
		case WM_MOUSEMOVE:
		{
			short xPos = GET_X_LPARAM(lParam);
			short yPos = GET_Y_LPARAM(lParam);
			gl->mouseMoveEvent(xPos, yPos);
			return 0;
		}
		case WM_MOUSEWHEEL:
		{
			int zDelta = GET_WHEEL_DELTA_WPARAM(wParam);
			gl->wheelEvent(zDelta);
			return 0;
        }
	}
	 // Pass All Unhandled Messages To DefWindowProc
    return DefWindowProc(hWnd,uMsg,wParam,lParam);
}

