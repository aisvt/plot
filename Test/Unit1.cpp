#include <vcl.h>
#pragma hdrstop

#include "Unit1.h"

#include <TestFramework.hpp>

class TTestT1 : public TTestCase
{
public:
  __fastcall virtual TTestT1(AnsiString name) : TTestCase(name) {}
  virtual void __fastcall SetUp() { }
  virtual void __fastcall TearDown() { }

__published:
  void __fastcall TestTxy();
};

void __fastcall TTestT1::TestTxy()
{
	std::vector<Txy> *v = new std::vector<Txy>();

	{
		const Txy txy = Txy(0.0,0.0);
		CheckEquals(0.0, txy.x);
		CheckEquals(0.0, txy.y);
	}
	{
		const Txy txy = Txy(1.0, -2.0);
		CheckEquals(1.0, txy.x);
		CheckEquals(-2.0, txy.y);
	}
}


static void registerTests()
{
  _di_ITestSuite iSuite;
  TTestSuite* testSuite = new TTestSuite("Testing basic Txy type");
  if (testSuite->GetInterface(iSuite)) {
	 testSuite->AddTests(__classid(TTestT1));
	 Testframework::RegisterTest(iSuite);
  } else {
	delete testSuite;
  }
}
#pragma startup registerTests 33
