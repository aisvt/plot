//---------------------------------------------------------------------------
// DUnit Project File.
//   Entry point of C++ project using DUnit framework.
//---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop

#include <XMLTestRunner.hpp>


int main()
{
	Xmltestrunner::RunRegisteredTests();
	return 0;
}

