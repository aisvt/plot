#include <vcl.h>
 #pragma hdrstop

 #include <TestFramework.hpp>

 class TTestTCalc : public TTestCase
 {
 public:
   __fastcall virtual TTestTCalc(AnsiString name) : TTestCase(name) {}
   virtual void __fastcall SetUp();
   virtual void __fastcall TearDown();

 __published:
   void __fastcall TestAdd();
   void __fastcall TestSub();
   void __fastcall Test3();
 };


 void __fastcall TTestTCalc::SetUp()
 {
 }

 void __fastcall TTestTCalc::TearDown()
 {
 }

 void __fastcall TTestTCalc::TestAdd()
 {
   // int Add(int x, int y)
 }

 void __fastcall TTestTCalc::TestSub()
 {
   // int Sub(int x, int y)
 }
  void __fastcall TTestTCalc::Test3()
 {
   int x = 1;
   CheckEquals(1,x);

 }

 static void registerTests()
 {
   _di_ITestSuite iSuite;
   TTestSuite* testSuite = new TTestSuite("Testing Unit7.h");
   if (testSuite->GetInterface(iSuite)) {
     testSuite->AddTests(__classid(TTestTCalc));
     Testframework::RegisterTest(iSuite);
   } else {
     delete testSuite;
   }
 }
 #pragma startup registerTests 33
