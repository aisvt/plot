#include <vcl.h>
#pragma hdrstop

#include "Unit3.h"

#include <TestFramework.hpp>


bool generateTests = false;

bool pixelsMatch(BYTE* bmpPixel, BYTE* glPixel)
{
	const int maxDiff = 2;
	const int diffR = bmpPixel[2] - glPixel[0];
	const int diffG = bmpPixel[1] - glPixel[1];
	const int diffB = bmpPixel[0] - glPixel[2];
    return abs(diffR) <= maxDiff && abs(diffG) <= maxDiff && abs(diffB) <= maxDiff;
}

void CheckRendering(int width, int height, UnicodeString name, void (*RenderingFunc)(KbPlot&), TTestCase* testCase)
{
	testCase->Check(CreateGLWindow(L"Test OpenGL Window", width, height, 16, false));

    GLWidget* glWidget = dynamic_cast<GLWidget*>(gl);
	KbPlot p(glWidget, -10.0, 50.0, -1.0, 50.0);
	RenderingFunc(p);
    glLineWidth(20);
	gl->GLpaint();
    glFinish();
    BYTE *rendered = new BYTE[width * height * 3];
    glPixelStorei(GL_PACK_ALIGNMENT, 1);
    glReadBuffer(GL_BACK);
    glReadPixels(0, 0, width, height, GL_RGB, GL_UNSIGNED_BYTE, rendered);
    glWidget->clearScene();

    Graphics::TBitmap *pBitmap = new Graphics::TBitmap();
    pBitmap->PixelFormat = pf24bit; // 24 bit BGR
	UnicodeString fileName = UnicodeString::Format("..\\images\\%s.bmp", &TVarRec(name), 1);

	if(generateTests)
	{
    	pBitmap->SetSize(width, height);
	}
	else
	{
		pBitmap->LoadFromFile(fileName);
		testCase->CheckEquals(width, pBitmap->Width);
		testCase->CheckEquals(height, pBitmap->Height);
		testCase->CheckEquals(pf24bit, pBitmap->PixelFormat);
	}

    BYTE *glPixel = rendered;
	int mismatchPixels = 0;
	const maxX = pBitmap->Width - 1, maxY = pBitmap->Height - 1;
	for(int y = maxY; y >= 0; y--)
    {
		BYTE *bmpPixel = (BYTE *)pBitmap->ScanLine[y];
        for(int x = 0; x <= maxX; x++)
        {
			if(generateTests)
			{
				bmpPixel[2] = glPixel[0];
				bmpPixel[1] = glPixel[1];
				bmpPixel[0] = glPixel[2];
			}
			else
			{
            	if(!pixelsMatch(bmpPixel, glPixel))
					mismatchPixels++;
			}
            bmpPixel += 3;
            glPixel += 3;
        }
	}
	if(generateTests)
		pBitmap->SaveToFile(fileName);

	delete pBitmap;
	delete[] rendered;
    KillGLWindow();

	if(!generateTests)
		testCase->CheckEquals(0, mismatchPixels, "Mismatch pixels");
}

void RenderingTest0Empty(KbPlot& p)
{
	// ���� ������ ���������
}

void RenderingTest1ByHand(KbPlot& p)
{
	std::vector<Txy> *v = new std::vector<Txy>();
	v->push_back(Txy(0.0,0.0));
	v->push_back(Txy(1.0,1.0));
	v->push_back(Txy(-1.0,1.0));
	v->push_back(Txy(2.0,4.0));
	v->push_back(Txy(-2.0,4.0));
	v->push_back(Txy(3.0,9.0));
	v->push_back(Txy(-3.0,9.0));
	DataSet ds(v);
	Style s;//  ------------
	s.lineColor = 0xAC0000FF ;  //-------------
	s.lineThickness = 3.0;//
	s.markerSize = 20;//
	s.markerColor = 0x00BFFF;  //
	s.markerType = Style::MARK_TRIANGLE;//
	s.lineStroke = Style::LINE_STD;//
	p.draw(ds,s);
}

void RenderingTest2Sequence(KbPlot& p)
{
	double data[256];
	for (int i = 0; i < 128; i += 2)
     {
		data[i] = i;
		data[i+1] = i*i*i;
	}
	DataSet ds1(RawData::get((unsigned char*)(&data[2]), 50, 16, 0, 8, RawData::TYPE_DOUBLE));
	Style s1;
	s1.lineColor = 0x800080;
	s1.lineThickness = 3.0;
	//p.toggleGrid(true);
	//p.setGridColor(0x00BFFF);
	//p.setGridYStroke(Style::LINE_DOTTED);
	std::vector<string>  strM;
	strM.push_back("1");
	strM.push_back("2");
	strM.push_back("3");
	strM.push_back("4");
	strM.push_back("5");
	strM.push_back("6");
	strM.push_back("7");
	p.draw(ds1, strM, s1);
}

void RenderingTest3Random(KbPlot& p)
{
	std::vector<Txy> *v1 = new std::vector<Txy>();
	for(int i = 0; i < 30; i++)
	{
		double k = 2.0 + rand() % 100;
		double m = 5.0 + rand() % 50;
		v1->push_back(Txy(k,m));
	}
	DataSet ds3(v1);
	Style s3;
	s3.lineColor = 0xAC0000FF;
	s3.lineThickness = 1.0;
	s3.markerSize = 8;
	s3.markerColor = 0x00BFFF;
	s3.markerType = Style::MARK_SQUARE;
	p.draw(ds3, s3);
}

class TTestRendering : public TTestCase
{
public:
  __fastcall virtual TTestRendering(AnsiString name) : TTestCase(name) {}
  virtual void __fastcall SetUp() { renderingTesting = true; }
  virtual void __fastcall TearDown() { renderingTesting = false; }

__published:
  void __fastcall Test0Empty();
  void __fastcall Test1ByHand();
  void __fastcall Test2Sequence();
  void __fastcall Test3Random();
};

void __fastcall TTestRendering::Test0Empty()
{
	CheckRendering(30, 20, "Test 0, empty", &RenderingTest0Empty, this);
}

void __fastcall TTestRendering::Test1ByHand()
{
	CheckRendering(30, 20, "Test 1, by hand", &RenderingTest1ByHand, this);
}

void __fastcall TTestRendering::Test2Sequence()
{
	CheckRendering(300, 150, "Test 2, sequence", &RenderingTest2Sequence, this);
}

void __fastcall TTestRendering::Test3Random()
{
	srand(1);
	CheckRendering(300, 150, "Test 3, random 1", &RenderingTest3Random, this);
	srand(12345678);
	CheckRendering(100, 50, "Test 3, random 2", &RenderingTest3Random, this);
    delete gl;
}


static void registerTests()
{
  _di_ITestSuite iSuite;
  TTestSuite* testSuite = new TTestSuite("Testing rendering");
  if (testSuite->GetInterface(iSuite)) {
	 testSuite->AddTests(__classid(TTestRendering));
	 Testframework::RegisterTest(iSuite);
  } else {
    delete testSuite;
  }
}
#pragma startup registerTests 33
